package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer getVar(String name) {
		return globalSymbols.getSymbol(name);
	}

	protected void createVar(String name) {
		globalSymbols.newSymbol(name);
	}

	protected Integer setVar(String name, Integer value) {
		globalSymbols.setSymbol(name, value);
		return value;
	}
	
	protected Integer div(Integer val1, Integer val2) throws RuntimeException{
		if (val2==0) {
			throw new RuntimeException("Nie da się dzielić przez zero");
		}
		return val1/val2;
	}
	
}
