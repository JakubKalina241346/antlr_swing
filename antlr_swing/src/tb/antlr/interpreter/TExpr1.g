tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

//prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;
prog    : (^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());} | e=expr | create)*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = (int) Math.pow($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(PODST i1=ID   e2=expr) {$out = setVar($i1.text, $e2.out);}
        | ID                       {$out = getVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
        
create 
    : ^(VAR i1=ID) {createVar($i1.text);} 
    ;